# R

## Content

```
./Radu Bercea:
Radu Bercea - Cele mai vechi upanisade.pdf

./Radu Stoichita:
Radu Stoichita - Natura conceptului in Logica lui Hegel.pdf

./Raoul Girardet:
Raoul Girardet - Nationalisme si natiune.pdf

./Raymond Aron:
Raymond Aron - Democratie si totalitarism.pdf
Raymond Aron - Istoria si dialectica violentei.pdf
Raymond Aron - Opiul intelectualilor.pdf

./Rene Descartes:
Rene Descartes - Discurs despre metoda de a ne conduce bine ratiunea si a cauta adevarul in stiinte.pdf
Rene Descartes - Meditatii metafizice.pdf
Rene Descartes - Meditationes de prima philosophia.pdf
Rene Descartes - Pasiunile sufletului.pdf
Rene Descartes - Reguli utile si clare.pdf

./Rene Guenon:
Rene Guenon - Autoritate spirituala si putere temporala.pdf
Rene Guenon - Criza lumii moderne.pdf
Rene Guenon - Ezoterismul crestin.pdf
Rene Guenon - Initiere si realizare spirituala.pdf
Rene Guenon - Metafizica si cosmologie orientala.pdf
Rene Guenon - Omul si devenirea sa dupa Vedanta.pdf
Rene Guenon - Scurta privire asupra initierii.pdf
Rene Guenon - Simbolismul crucii.pdf
Rene Guenon - Simboluri ale stiintei sacre.pdf
Rene Guenon - Starile multiple ale fiintei.pdf

./Rene Taton:
Rene Taton - Istoria generala a stiintei, vol. 1 (Stiinta antica si medievala).pdf
Rene Taton - Istoria generala a stiintei, vol. 3 (Stiinta contemporana).pdf

./Reynal Sorel:
Reynal Sorel - Orfeu si orfismul.pdf

./Richard Dagger & Terrence Ball:
Richard Dagger & Terrence Ball - Ideologii politice si idealul democratic.pdf

./Richard Dawkins:
Richard Dawkins - Ceasornicarul orb.pdf
Richard Dawkins - Un rau pornit din Eden.pdf

./Richard Feynman:
Richard Feynman - Despre caracterul legilor fizicii.pdf
Richard Feynman - QED.pdf
Richard Feynman - Sase lectii usoare.pdf

./Richard Leakey:
Richard Leakey - Originea omului.pdf

./Richard Mervyn Hare:
Richard Mervyn Hare - Platon (Maestrii spiritului).pdf

./Richard Rorty:
Richard Rorty - Contingenta, ironie si solidaritate.pdf
Richard Rorty - Eseuri filosofice, vol. 1.pdf

./Richard Schusterman:
Richard Schusterman - Estetica Pragmatista.pdf

./Robert Alan Dahl:
Robert Alan Dahl - Despre democratie.pdf
Robert Alan Dahl - Poliarhiile.pdf

./Robert David Putnam & Robert Leonardi & Rafaella Y. Nanetti:
Robert David Putnam & Robert Leonardi & Rafaella Y. Nanetti - Cum functioneaza democratia.pdf

./Robert Heilbroner:
Robert Heilbroner - Filosofii lucrurilor pamantesti.pdf

./Robert Muchembled:
Robert Muchembled - Magia si vrajitoria in Europa din Evul Mediu pana Astazi.pdf

./Robert Nozick:
Robert Nozick - Anarhie stat si utopie.pdf

./Robert Park:
Robert Park - Stiinta voodoo.pdf

./Robert Wolke:
Robert Wolke - Ce i-a spus Einstein bucatarului sau.pdf

./Robert Zimmer:
Robert Zimmer - Filosofia de la Iluminism pana astazi.pdf

./Robin George Collingwood:
Robin George Collingwood - Ideea de natura.pdf

./Roger Penrose:
Roger Penrose - Incertitudinile ratiunii, umbrele mintii.pdf
Roger Penrose - Mintea noastra cea de toate zilele.pdf

./Roger Scruton:
Roger Scruton - Cultura moderna pe intelesul oamenilor inteligenti.pdf
Roger Scruton - Kant (Maestrii spiritului).pdf
Roger Scruton - Spinoza (Maestrii spiritului).pdf

./Roy Baumeister:
Roy Baumeister - Sensuri ale vietii.pdf

./Rudiger Safranski:
Rudiger Safranski - Schopenhauer si anii salbatici ai filosofiei.pdf

./Rudolf Carnap:
Rudolf Carnap - Semnificatie si necesitate.pdf
Rudolf Carnap - Vechea si noua logica.pdf

./Rudolf Peierls:
Rudolf Peierls - Legile naturii.pdf
```

